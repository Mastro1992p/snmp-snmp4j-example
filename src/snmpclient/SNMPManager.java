
package snmpclient;


import java.io.IOException;

import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.Target;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;

/**
 *
 * @author Mastro
 */
public class SNMPManager {

    Snmp snmp = null;
    String address = null;

/**
* Constructor
* @param add
*/
public SNMPManager(String add){
    address = add;
}

public static void main(String[] args) throws IOException {
/**
* Port 161 usado para lectura y operaciones no TRAP
* Port 162 usado para la generacion de TRAPs
*/
    SNMPManager client = new SNMPManager("udp:127.0.0.1/161");
    client.start();
/**
* OID - .1.3.6.1.2.1.1.1.0 => SysDec
* OID - .1.3.6.1.2.1.1.5.0 => SysName
* => Segun el tutorial es util usar un explorador de MIBs, esto para ver los MIBS existentes en el sistema
*/                                               
    String sysDescr = client.getAsString(new OID(".1.3.6.1.2.1.1.1.0"));
    String sysDescr2 = client.getAsString(new OID(".1.3.6.1.2.1.1.5.0"));
    System.out.println(sysDescr);
    System.out.println(sysDescr2);
}

/**
* Start inicia de manera asyncrona la escucha de respuestas SNMP, sin esto no es posible obtener respuesta
* @throws IOException
*/
    public void start() throws IOException {
    TransportMapping transport = new DefaultUdpTransportMapping();
    snmp = new Snmp(transport);
// Do not forget this line!
    transport.listen();
}

/**
*  Metodo que toma un OID object ID y que retorna como respuesta un String proveniente del agente.
*  Para esta prueba va contra el servicio local en la maquina.
* @param oid
* @return
* @throws IOException
*/
public String getAsString(OID oid) throws IOException {
    ResponseEvent event = get(new OID[] { oid });
    return event.getResponse().get(0).getVariable().toString();
}

/**
* Tiene el mismo comportamiento que getAsString(OID oid), sin embargo este maneja multiples OID
* Notar el arreglo OID oids[]
* @param oids
* @return
* @throws IOException
*/
public ResponseEvent get(OID oids[]) throws IOException {
    
    PDU pdu = new PDU();
    for (OID oid : oids) {
        pdu.add(new VariableBinding(oid));
    }
    
    pdu.setType(PDU.GET);
    ResponseEvent event = snmp.send(pdu, getTarget(), null);
    
    if(event != null) {
        return event;
    }
    throw new RuntimeException("GET timed out");
}

/**
* Este metodo retorno un Target(obejtivo), el cual contiene informacion de
* como y donde deberia ser buscada la DATA.
* @return
*/
private Target getTarget() {
    
    Address targetAddress = GenericAddress.parse(address);
    CommunityTarget target = new CommunityTarget();
    target.setCommunity(new OctetString("public"));
    target.setAddress(targetAddress);
    target.setRetries(2);
    target.setTimeout(1500);
    target.setVersion(SnmpConstants.version2c);

    return target;
}
    
}
