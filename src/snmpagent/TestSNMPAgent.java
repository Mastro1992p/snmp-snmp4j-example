
package snmpagent;

import java.io.IOException;

import org.snmp4j.smi.OID;

import snmpclient.SNMPManager;

/**
 *
 * @author Mastro
 */
public class TestSNMPAgent {

        SNMPAgent agent = null;
	/**
	 * Esta es la clase en el package snmpclient, el cual hace de Manager-Cliente
	 */
	SNMPManager client = null;

	String address = null;
    
   	static final OID sysDescr = new OID(".1.3.6.1.2.1.1.1.0");

	public static void main(String[] args) throws IOException {
		TestSNMPAgent client = new TestSNMPAgent("udp:127.0.0.1/161");
		client.init();
	}

	/**
	 * Constructor
	 *
	 * @param add
	 */
	public TestSNMPAgent(String add) {
		address = add;
	}

	private void init() throws IOException {
		agent = new SNMPAgent("0.0.0.0/2001");
		agent.start();

		// Debemos limpiar de registro los mibs creados por default
		// Para ello se debe hacer override al metodo de registro y luego se registra los MIBS requeridos
		agent.unregisterManagedObject(agent.getSnmpv2MIB());

		// Registra un System Description, se puede usar alguno local de la red. para este caso use los recursos
		// de la maquina donde se ejecute este codigo, es posible bajo las configuraciones correctas extraer de otro entorno
		agent.registerManagedObject(MOCreator.createReadOnly(sysDescr,
                                        "NEW values CORE-COUNT "+Runtime.getRuntime().availableProcessors()+
                                        " MAX-MEM "+Runtime.getRuntime().maxMemory()+
                                        " TOTAL-MEM "+Runtime.getRuntime().totalMemory()));

		// Configuracion de un client-manager para testear el agente creado
		client = new SNMPManager("udp:127.0.0.1/2001");
		client.start();
		// recuperacion del valor creado por el agente y desplegado por el cliente
		System.out.println(client.getAsString(sysDescr));
	}
    
}
